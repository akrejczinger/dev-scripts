Scripts
=======

A collection of my personal scripts from the home folder

Installation
------------

1. Check the URL by clicking on the "Clone or download" button in the top right.
2. Run `homesick clone <clone_URL>`
3. Run `homesick symlink scripts`
4. The scripts can be ran from the shell just by typing their name.
