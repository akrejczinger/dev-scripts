#!/usr/bin/env python
"""
This script takes a filename and opens the conflicted versions in a 3-way diff.
"""

from glob import glob
from os.path import isfile
import subprocess
import sys

if len(sys.argv) != 2:
    print 'Wrong number of arguments. Usage: vimresolve.py filename'
    print 'There should be three files at the same path as filename:'
    print '* filename.working'
    print '* filename.merge-left.rXXXXX'
    print '* filename.merge-right.rXXXXX'
    exit(1)

filename = sys.argv[1]

# Find the three conflicted versions of the file
mine = filename + '.working'
left = glob(filename + '.merge-left*')
right = glob(filename + '.merge-right*')

# check if the working version file actually exists
if not isfile(mine):
    print 'Working version of file not found'
    exit(1)

# Check that there is exactly one file matching each glob
if len(left) > 1:
    print 'Multiple files found for glob: {0}'.format(', '.join(left))
    exit(1)
elif len(left) == 0:
    print 'Left version not found'
    exit(1)
else:
    left = left[0]

if len(right) != 1:
    print 'Multiple files found for glob: {0}'.format(', '.join(right))
    exit(1)
elif len(right) == 0:
    print 'Right version not found'
    exit(1)
else:
    right = right[0]


cmd = "nvim -d {left} {mine} {right}".format(
    left=left, mine=mine, right=right)
subprocess.call(cmd, shell=True)
