#!/bin/bash

# Set up a new workspace with clock on bottom right
# export TERM=xterm-256color # this fixes colors but breaks function keys
tmux new -d
tmux splitw -v -l 6
tmux splitw -h
tmux clock
tmux select-pane -t :.1

tmux attach
